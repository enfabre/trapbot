var cheerio = require('cheerio');
var request = require('request');

module.exports = class Counter {
  getChampInfo(champ) {
    return new Promise(function(resolve, reject) {
      request(`http://www.lolcounter.com/champions/${champ}`, function(error, response, body) {
        if (!error && response.statusCode == 200) {
          let $ = cheerio.load(body);

          let column2 = [];
          let column4 = [];
          var name = $('div[class=name]').html();

          $('div[class="col4 left"]').contents().each(function(i, elem) {
            column4.push($(this).text().trim());
          });
          column4 = column4.filter(function(column4) {
            return (column4 !== (undefined || ''));
          });

          $('div[class="col2 left"]').contents().each(function(i, elem) {
            column2.push($(this).text().trim());
          });
          column2 = column2.filter(function(column2) {
            return (column2 !== (undefined || ''));
          });

          var res = `
__**${name}**__
**Health Points:** _${column2[0]}_
**Health Regen:** _${column4[0]}_
**Mana:** _${column2[1]}_
**Mana Regen:** _${column4[1]}_
**Damage:** _${column2[2]}_
**Attack Range:** _${column2[3]}_
**Attack Spd:** _${column4[3]}_
**Armor:** _${column2[4]}_
**Movement Spd:** _${column4[4]}_
`;
          resolve(res);
        } else {
          reject("Not a champion.")
        }
      });
    });
  }

  getWeakStrongAgainst(champ) {
    return new Promise(function(resolve, reject) {
      request(`http://www.lolcounter.com/champions/${champ}`, function(error, response, body) {
        if (!error && response.statusCode == 200) {
          let $ = cheerio.load(body);
          var name = $('div[class=name]').html();
          let weak = [];
          let strong = [];

          $('div[class="weak-block"]').contents().each(function(i, elem) {
            weak.push($(this).find($('div[class="name"]')).text());
          });
          weak = weak.filter(function(weak) {
            return (weak !== (undefined || ''));
          });

          $('div[class="strong-block"]').contents().each(function(i, elem) {
            strong.push($(this).find($('div[class="name"]')).text());
          });
          strong = strong.filter(function(strong) {
            return (strong !== (undefined || ''));
          });

          weak = weak.slice(0, 5);
          strong = strong.slice(0, 5);

          var res = `
__**${name} is:**__

**Weak Against**
${weak[0]}
${weak[1]}
${weak[2]}
${weak[3]}
${weak[4]}

**Strong Against**
${strong[0]}
${strong[1]}
${strong[2]}
${strong[3]}
${strong[4]}
`;
          resolve(res);
        } else {
          reject("Not a champion.")
        }
      });
    });
  }
}
