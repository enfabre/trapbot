const console = require('better-console');
const Helper = require('./helpers.js')
const Urban = require('./urbandict.js');
var clui = require('clui');

// Helper Req's
const toTime = Helper.toTime;
const listUsers = Helper.listUsers;
const actualUsersSize = Helper.actualUsersSize;
const notifyGameAll = Helper.notifyGameAll;

/**
 * Adds a music feature to your bot.
 * @param  {Client} bot     This the starting point Bot.
 * @param  {object} options Misc Options undefined for now
 * @return {Void}
 */
module.exports = function(bot, options) {
  // Get all options.
  let PREFIX = (options && options.prefix) || ';';

  // Create an object of queue.
  let queue = {};

  bot.on('message', msg => {
    const message = msg.content.trim();

    // Check if the message is a command.
    if (message.startsWith(PREFIX)) {
      // Get the command and suffix.
      const command = message.split(/[ \n]/)[0].substring(PREFIX.length).toLowerCase().trim();
      const suffix = message.substring(PREFIX.length + command.length).trim();

      // Process the commands.
      switch (command) {
        case 'ping':
          return ping(msg, suffix);
        case 'avatar':
          return avatar(msg, suffix);
        case 'purge':
          return purge(msg, suffix);
        case 'help':
          return help(msg, suffix);
        case 'listguilds':
          return listguilds(msg, suffix);
        case 'serverinfo':
          return serverinfo(msg, suffix);
        case 'shutdown':
          return shutdown(msg, suffix);
        case 'test':
          return setStatus(msg, suffix);
        case 'define':
          return urbanDefine(msg, suffix);
        case 'tester':
          return tester(msg, suffix);
      // case 'resetstatus':
      //   return resetStatus(msg, suffix);
      }
    }
  });

  function urbanDefine(msg, suffix) {
    const u = new Urban();
    u.define(suffix).then(function(sol) {
      return msg.reply(`
__**UrbanDictionary**__

**Definition:** ${sol.definition}
**URL:** ${sol.permalink}
`)
    });
  }

  function tester(msg, suffix) {
    var Progress = clui.Progress;
    // var maxVal;
    var thisPercentBar = new Progress(20);

    var seekBar = thisPercentBar.update(.5).split(' ')[0];
    msg.channel.sendMessage(seekBar);
  }

  function setStatus(msg, suffix) {
    console.log(suffix);
  }

  function help(msg, suffix) {
    msg.author.sendMessage(`Whats up Trapper, my name is ** ${bot.user}**!
  I was invited to your server **${msg.channel.guild.name}** by the server owner.

  __**Chief Commands**__
  ${PREFIX}serverinfo
  ${PREFIX}ping
  ${PREFIX}purge

  __**Basic Commands**__
  ${PREFIX}help
  ${PREFIX}avatar

  __**Music Commands**__
  ${PREFIX}play
  ${PREFIX}queue
  ${PREFIX}shuffle
  ***(Requires DJ role)***
  ${PREFIX}repeat
  ${PREFIX}pause
  ${PREFIX}resume
  ${PREFIX}volume [low, med, high, number]
  ${PREFIX}skip
  ${PREFIX}stop                                                                                                                                                                         
  ${PREFIX}mute
  ${PREFIX}ummute`);
  }

  /**
   * Returns information about the server the message was sent in.
   * @param  {[type]} msg    [description]
   * @param  {[type]} suffix [description]
   * @return {[type]}        [description]
   */
  function serverinfo(msg, suffix) {
    if (!isChief(msg.author)) {
      msg.channel.sendMessage(`Not Chief`)
    }

    msg.author.sendMessage(`**Server Information**
    Server Name: ${msg.channel.guild.name}
    Server ID: ${msg.channel.guild.id}
    Server Owner: ${msg.channel.guild.owner.user}
    Server Created: ${msg.channel.guild.creationDate.toUTCString()}`);
  }

  function avatar(msg, suffix) {
    let mentioneduser = msg.mentions.users.first();
    let userOG = msg.author;
    let aURL;

    if (mentioneduser === undefined) {
      aURL = userOG.avatarURL;
    } else {
      aURL = mentioneduser.avatarURL;
    }
    if (aURL === null) return msg.channel.sendMessage(`**User has no avatar URL**`);
    msg.channel.sendMessage(aURL);
  }

  function listguilds(msg, suffix) {
    if (!isChief(msg.author)) {
      msg.channel.sendMessage(`Not Chief`)
    }

    let guildList = bot.guilds.array();
    for (i = 0; i < guildList.length; i++) {
      msg.channel.sendMessage(`**${i}. ${guildList[i].name}**`);
    }
  }

  function purge(msg, suffix) {
    let number = suffix;
    var num = 99;
    num = parseInt(number) + 1;

    msg.channel.fetchMessages({
      limit: num
    }).then(messages => {
      msg.channel.bulkDelete(messages);
      msg.channel.sendMessage(`Removed ${messages.size - 1} Messages`)
        .then(newMessage => newMessage.delete(3000));
    });
  }

  function ping(msg, suffix) {
    if (!isChief(msg.author)) {
      msg.channel.sendMessage(`Not Chief`)
    }

    msg.channel.sendMessage('Hi Chief, ' + msg.author + ', I am ' +
      bot.user + "\n" + "You are on **" + msg.channel.guild.name + "**" +
      "\n" + " I have been online for " + toTime(bot.uptime));
  }

  function shutdown(msg, suffix) {
    if (!isChief(msg.author)) {
      msg.channel.sendMessage(`Not Chief`)
    }

    msg.channel.sendMessage(`**See ya later, Bot shutting down...**`);
    bot.destroy();
    setTimeout(() => {
      process.exit();
    }, 1000);
  }

  function isChief(chief) {
    let chiefBool = false;
    if (chief.id == 170702846260412416) {
      chiefBool = true;
    }
    return chiefBool;
  }

}
