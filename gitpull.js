const git = require('simple-git');

git()
  .exec(() => console.log('Starting pull...'))
  .pull('origin', 'master')
  .exec(() => console.log('pull done.'));
