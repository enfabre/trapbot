const console = require('better-console');
const yt = require('ytdl-core');
const ytSearch = require('youtube-search');
const ypi = require("youtube-playlist-info");
const aShuffle = require("array-shuffle");
const mysql = require('mysql');
const secToMin = require('sec-to-min');

/**
 * Adds a music feature to your bot.
 * @param  {Client} bot     This the starting point Bot.
 * @param  {object} options Misc Options undefined for now
 * @return {Void}
 */
module.exports = function(bot, options) {

  let CONFIG = JSON.parse(options.config);
  let YOUTUBE = CONFIG['YOUTUBE'];
  let ytApi = YOUTUBE.token;


  // Get all options.
  let PREFIX = (options && options.prefix) || ';';
  let MAX_QUEUE_SIZE = (options && options.maxQueueSize) || 20;
  // Create an object of queues.
  let queues = {};

  //
  var opts = {
    key: ytApi,
    type: "video",
  };

  bot.on('message', msg => {
    const message = msg.content.trim();

    // Check if the message is a command.
    if (message.startsWith(PREFIX)) {
      // Get the command and suffix.
      const command = message.split(/[ \n]/)[0].substring(PREFIX.length).toLowerCase().trim();
      const suffix = message.substring(PREFIX.length + command.length).trim();

      // Process the commands.
      switch (command) {
        case 'play':
          return play(msg, suffix);
        case 'queue':
          return getQueue(msg, suffix);
        case 'shuffle':
          return shuffle(msg, suffix);
        case 'clear':
          return clearSongs(msg, suffix);
        // Server Playlist commands
        case 'addsong':
          return addToListEnd(msg, suffix);
        case 'addsongto':
          return addToListPos(msg, suffix);
        case 'delsong':
          return removeFromList(msg, suffix);
        case 'showplaylist':
          return showPlaylist(msg, suffix);
        case 'clearPlaylist':
          return clearPlaylist(msg, suffix);
      }
    }
  });

  /**
   * This function executes the next song in the queues.
   * @param  {message} msg    Represents a discord message.
   * @param  {message} suffix Represents the name of the song.
   * @return {void}
   */
  function playQueue(msg, suffix) {
    // Checks if queues already playing
    if (queues[msg.guild.id].playing) {
      return;
    }
    const voiceChannel = msg.member.voiceChannel;
    var repeatEnabled = false;
    var stop = false;
    var isMuted = false;
    var currVol;
    (
      /**
       * This nested funtion plays the song and shifts the song queues.
       * @param  {queue} song Represents the song queue which is defined in play()
       * @return {void}
       */
      function playSong(song) {
      queues[msg.guild.id].playing = true;
      if (song === undefined) {
        msg.channel.sendMessage('Queue is empty');
        queues[msg.guild.id].playing = false;
        msg.member.voiceChannel.leave();
        return;
      }
      msg.channel.sendMessage(`:musical_note: Playing: **${song.title}**`);
      voiceChannel.join().then(myVoiceConnection => {
        console.log("Connected to voice channel.");
        let stream = yt(song.url, {
          audioonly: true
        });

        let dispatcher = myVoiceConnection.playStream(stream);
        dispatcher.setVolume(.10);
        let collector = msg.channel.createCollector(m => m);
        collector.on('message', m => {
          if (m.content.startsWith(PREFIX + 'pause')) {
            if (isDJ(m.guild.member(m.author)) || isChief(m.author)) {
              dispatcher.pause();
              msg.channel.sendMessage(':play_pause: **paused**. Use ;resume to continue playing.');
            } else {
              m.channel.sendMessage("**Not a DJ**");
            }
          } else if (m.content.startsWith(PREFIX + 'resume')) {
            if (isDJ(m.guild.member(m.author)) || isChief(m.author)) {
              dispatcher.resume();
              msg.channel.sendMessage(':play_pause: **resumed**');
            } else {
              m.channel.sendMessage("**Not a DJ**")
            }
          } else if (m.content.startsWith(PREFIX + 'resume')) {
            if (isDJ(m.guild.member(m.author)) || isChief(m.author)) {
              dispatcher.resume();
              msg.channel.sendMessage(':play_pause: **resumed**');
            } else {
              m.channel.sendMessage("**Not a DJ**")
            }
          } else if (m.content.startsWith(PREFIX + 'skip')) {
            if (isDJ(m.guild.member(m.author)) || isChief(m.author)) {
              dispatcher.end();
              msg.channel.sendMessage(':track_next: **skipped**');
            } else {
              m.channel.sendMessage("**Not a DJ**")
            }
          } else if (m.content.startsWith(PREFIX + 'stop')) {
            if (isDJ(m.guild.member(m.author)) || isChief(m.author)) {
              stop = true;
              queues = {};
              dispatcher.end("User Request");
              msg.channel.sendMessage(':stop_button: **stopped**');
            } else {
              m.channel.sendMessage("**Not a DJ**")
            }
          } else if (m.content.startsWith(PREFIX + 'repeat')) {
            if (isDJ(m.guild.member(m.author)) || isChief(m.author)) {
              if (repeatEnabled === true) {
                repeatEnabled = false;
                return msg.channel.sendMessage(`Repeat has been **Disabled**`);
              }
              repeatEnabled = true;
              return msg.channel.sendMessage(`:repeat_one: Repeat **Enabled** for **${song.title}**`);
            } else {
              m.channel.sendMessage("**Not a DJ**")
            }
          } else if (m.content.startsWith(PREFIX + 'getvol')) {
            if (isDJ(m.guild.member(m.author)) || isChief(m.author)) {
              msg.channel.sendMessage(`Volume is currently **${dispatcher.volume * 50}%**`);
            } else {
              m.channel.sendMessage("**Not a DJ**")
            }
          } else if (m.content.startsWith(PREFIX + 'volume')) {
            if (isDJ(m.guild.member(m.author)) || isChief(m.author)) {
              const com = m.content.trim().split(/[ \n]/)[0].substring(PREFIX.length).toLowerCase().trim();
              const volOption = m.content.trim().substring(PREFIX.length + com.length).trim();
              var volSym = "";
              if (isNaN(parseInt(volOption))) {
                switch (volOption) {
                  case 'low':
                    dispatcher.setVolume(.25);
                    volSym = ":speaker:";
                    break;
                  case 'med':
                    dispatcher.setVolume(.5);
                    volSym = ":sound:";
                    break;
                  case 'high':
                    dispatcher.setVolume(1);
                    volSym = ":loud_sound:";
                    break;
                  case 'max':
                    dispatcher.setVolume(2);
                    volSym = ":mega:";
                    break;
                  default:
                    return msg.channel.sendMessage(`Please select low, med, high or max`)
                }
                return msg.channel.sendMessage(`${volSym} Volume set to **${volOption.toUpperCase()}**`);
              }
              if (volOption < 0 || volOption > 100) {
                return msg.channel.sendMessage(`**Please select a value between 0 and 100**`);
              }
              dispatcher.setVolume(volOption / 50);
              msg.channel.sendMessage(`Volume: ${Math.round(dispatcher.volume * 50)}%`);
            } else {
              m.channel.sendMessage("**Not a DJ**")
            }
          } else if (m.content.startsWith(PREFIX + 'mute')) {
            if (isDJ(m.guild.member(m.author)) || isChief(m.author)) {
              if (isMuted) {
                dispatcher.setVolume(currVol);
                msg.channel.sendMessage(`:sound: Volume: **Unmuted**`);
                isMuted = false;
              } else {
                currVol = dispatcher.volume;
                dispatcher.setVolume(0);
                msg.channel.sendMessage(`:mute: Volume: **Muted**`);
                isMuted = true;
              }
            } else {
              m.channel.sendMessage("**Not a DJ**")
            }
          } else if (m.content.startsWith(PREFIX + 'np')) {
            let musicPlayer = getMusicPlayer(dispatcher.time, song.requestor,
              song.info, "radio_button");

            msg.channel.sendMessage(`Now Playing:musical_note:
 ${musicPlayer.title}`);
          }
        });

        // These event handlers determine what play does on end or error.
        dispatcher.on('end', () => {
          if (stop) {
            collector.stop();
            return msg.member.voiceChannel.leave();
          } else if (repeatEnabled) {
            console.log(`Repeat is ${repeatEnabled}`);
            setTimeout(() => {
              collector.stop();
              playSong(queues[msg.guild.id].songs[0]);
            }, 500);
            return;
          } else {
            setTimeout(() => {
              collector.stop();
              queues[msg.guild.id].songs.shift();
              playSong(queues[msg.guild.id].songs[0]);
            }, 1000);
            return;
          }
        });
        dispatcher.on('error', (err) => {
          collector.stop();
          queues[msg.guild.id].songs.shift();
          queues[msg.guild.id].playing = false;
          msg.channel.sendMessage('error: ' + err);
          msg.member.voiceChannel.leave();
          return;
        });
      // dispatcher.on('debug', (information) => {
      //   if (information.includes("Triggered terminal")) {
      //     console.info(information);
      //     setTimeout(() => {
      //       collector.stop();
      //       queues[msg.guild.id].songs.shift();
      //       playSong(queues[msg.guild.id].songs[0]);
      //     }, 500);
      //   }
      //
      // });
      // dispatcher.on('speaking', (value) => {
      //   msg.channel.sendMessage(value);
      // });
      // dispatcher.on('start', () => {
      //   msg.channel.sendMessage("**Streaming**");
      // });
      }).catch(console.error);
    })(queues[msg.guild.id].songs[0]);
  }

  /**
   * This function searches for the requested song and adds it next in the
   * queues.
   * @param  {message} msg    Represents a discord message
   * @param  {message} suffix Represents the name of the song.
   * @return {void}
   */
  function play(msg, suffix) {
    if (!msg.member.voiceChannel) {
      return msg.channel.sendMessage(`:warning:**Please join a voice channel first**`);
    }
    if (suffix.includes("playlist")) {
      var PLAYLISTID = suffix.trim().split("list=");
      ypi.playlistInfo(ytApi, PLAYLISTID[1], function(playlistItems) {
        var count = 0;
        for (i = 0; i < playlistItems.length - 1; i++) {
          var url = "https://www.youtube.com/watch?v=" + playlistItems[i].resourceId.videoId;
          var title = playlistItems[i].title;
          yt.getInfo(url, function(err, info) {
            if (!queues.hasOwnProperty(msg.guild.id)) {
              queues[msg.guild.id] = {};
              queues[msg.guild.id].serverName = msg.guild.name;
              queues[msg.guild.id].playing = false;
              queues[msg.guild.id].songs = [];
            }
            queues[msg.guild.id].songs.push({
              url: url,
              title: title,
              requestor: msg.author,
              info: info
            });
          });
          count++;
          msg.channel.sendMessage(`added **${count}** songs to playlist`);
          playQueue(msg, suffix);
        }
      });
      return;
    } else if (suffix.includes("watch")) {
      ytSearch(suffix, opts, function(err, results) {
        if (err) msg.channel.sendMessage('Invalid YouTube Link: ' + err);
        let songNode = results[0];
        var title = songNode.title;
        var id = songNode.id;
        var url = "https://www.youtube.com/watch?v=" + id;
        yt.getInfo(url, function(err, info) {
          //Initializes new Queue
          if (!queues.hasOwnProperty(msg.guild.id)) {
            queues[msg.guild.id] = {};
            queues[msg.guild.id].serverName = msg.guild.name;
            queues[msg.guild.id].playing = false;
            queues[msg.guild.id].songs = [];
          }
          if (queues[msg.guild.id].songs.length >= MAX_QUEUE_SIZE) {
            return msg.channel.sendMessage(`**Max Queue Size Has Been Reached**`);
          }
          queues[msg.guild.id].songs.push({
            url: url,
            title: title,
            requestor: msg.author,
            info: info
          });
          msg.channel.sendMessage(`added **${title}** to the queue`);
          playQueue(msg, suffix);
        });
      });
    } else {
      ytSearch(suffix += " Lyrics", opts, function(err, results) {
        if (err) msg.channel.sendMessage('Invalid YouTube Link: ' + err);
        let songNode = results[0];
        var title = songNode.title;
        var id = songNode.id;
        var url = "https://www.youtube.com/watch?v=" + id;
        yt.getInfo(url, function(err, info) {
          //Initializes new Queue
          if (!queues.hasOwnProperty(msg.guild.id)) {
            queues[msg.guild.id] = {};
            queues[msg.guild.id].serverName = msg.guild.name;
            queues[msg.guild.id].playing = false;
            queues[msg.guild.id].songs = [];
          }
          if (queues[msg.guild.id].songs.length >= MAX_QUEUE_SIZE) {
            return msg.channel.sendMessage(`**Max Queue Size Has Been Reached**`);
          }
          queues[msg.guild.id].songs.push({
            url: url,
            title: title,
            requestor: msg.author,
            info: info
          });
          msg.channel.sendMessage(`added **${title}** to the queue`);
          playQueue(msg, suffix);
        });
      });
    }

  }

  /**
   * This function returns the current items in the queue.
   * @param  {message} msg    Represents a discord message.
   * @param  {message} suffix Represents the content of a discord message.
   * @return {void}
   */
  function getQueue(msg, suffix) {
    let currQueue = queues[msg.guild.id];
    let outQueue = " ";

    if (currQueue === undefined) {
      return msg.channel.sendMessage(`Queue is empty`);
    }
    msg.channel.sendMessage(`**${currQueue.songs.length}** Songs currently in **${currQueue.serverName}** Queue.`);
    for (i = 0; i < currQueue.songs.length; i++) {
      outQueue += (`${i + 1}: **${currQueue.songs[i].title}**
`);
    }
    msg.channel.sendMessage(outQueue);
  }

  /**
   * This function shuffles the playlist.
   * @param  {message} msg    Represents a discord message.
   * @param  {message} suffix Represents the content of a discord message.
   * @return {void}
   */
  function shuffle(msg, suffix) {
    let currQueue = queues[msg.guild.id];
    if (currQueue === undefined) {
      return msg.channel.sendMessage(`**Nothing to shuffle: No songs**`);
    } else if (currQueue.songs.length < 3) {
      return msg.channel.sendMessage(`**Nothing to shuffle: Less than 3 songs**`);
    } else {
      let currSong = currQueue.songs.shift();
      queues[msg.guild.id].songs = aShuffle(currQueue.songs);
      queues[msg.guild.id].songs.unshift(currSong)
      msg.channel.sendMessage(`**Queue Shuffled**`);
    }
  }

  /**
   * This function clears the playlist, but retains the currently playing song.
   * @param  {message} msg    Represents a discord message.
   * @param  {message} suffix Represents the content of a discord message.
   * @return {void}
   */
  function clearSongs(msg, suffix) {
    let currQueue = queues[msg.guild.id];
    if (currQueue === undefined || currQueue.songs.length < 2) {
      return msg.channel.sendMessage(`**Nothing to clear, if a song playing just use ${PREFIX}stop**`);
    } else {
      let currSong = currQueue.songs.shift();
      queues[msg.guild.id].songs = [];
      queues[msg.guild.id].songs.push(currSong);
      msg.channel.sendMessage(`**Queue Cleared**`);
    }
  }

  /**
   * Returns whether or not member is a DJ
   * @param  {GuildMember} member Discord guild member
   * @return {boolean}        An array of discord roles.
   */
  function isDJ(member) {
    let djBool = false;
    member.roles.forEach((role) => {
      if (role.name.trim() === "DJ") {
        djBool = true;
      }
    });
    return djBool;
  }

  function isChief(chief) {
    let chiefBool = false;
    if (chief.id == 170702846260412416) {
      chiefBool = true;
    }
    return chiefBool;
  }

  function getMusicPlayer(currVal, requestor, info, seekButton) {
    currVal = (currVal / 1000).toFixed(0);
    var maxVal = parseInt(info.length_seconds, 10);
    var seekRect = "▬";
    var timeElapsed = secToMin(currVal) + " / " + secToMin(maxVal);
    var seekBar = "";
    var percentage = (currVal / maxVal).toFixed(1) * 10;

    for (var i = 0; i < 10; i++) {
      if (i == percentage) {
        seekBar += seekButton;
      } else {
        seekBar += seekRect;
      }
    }

    return {
      title: info.title,
      seekBar: seekBar,
      timeElapsed: timeElapsed,
      requestor: requestor,
      thumbnail: info.thumbnail_url.replace("default.jpg", "hqdefault.jpg"),
      link: "https://www.youtube.com/watch?v=" + info.video_id
    };
  }
}
