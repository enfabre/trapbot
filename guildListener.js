const console = require('better-console');
const mysql = require('mysql');

/**
 * Adds a guildListener plugin to your bot.
 * @param  {Client} bot     This the starting point Bot.
 * @param  {object} options Misc Options undefined for now
 * @return {Void}
 */
module.exports = function(bot, options) {
  let PREFIX = (options && options.prefix) || ';';

  bot.on("guildCreate", function(server) {
    console.log("Trying to insert server " + server.name + " into database");
    var info = {
      "servername": "'" + server.name + "'",
      "serverid": server.id,
      "ownerid": server.owner.id,
      "prefix": ";"
    }
  });

  bot.on("guildDelete", function(server) {
    console.log("Attempting to remove " + server.name + " from the database");
  });

  bot.on("guildMemberAdd", (server, member) => {
    console.log(`New User "${member.user.username}" has joined "${server.name}"`);
    server.defaultChannel.sendMessage(`${member.user} has **joined** the server`);
  });

  bot.on('guildMemberRemove', (server, member) => {
    console.log(`User "${member.user.username}" has been kicked from "${server.name}"`);
    server.defaultChannel.sendMessage(`That scrub ${member.user} has been **kicked**`);
  });

  bot.on('guildBanAdd', (server, member) => {
    server.defaultChannel.sendMessage(`Someone **Banned** ${member}`);
  });

  bot.on('guildBanRemove', (server, member) => {
    server.defaultChannel.sendMessage(`${member} has had his **Ban** lifted`);
  });
}
