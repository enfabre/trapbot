// Discord Client requires these
const console = require('better-console');
const fs = require("fs-extra");
const Discord = require('discord.js');
const client = Discord.Client;

// Grab env variables from config.
const config = JSON.parse(fs.readFileSync('./config.json'));

// Create a new bot.
const bot = new client();
const token = config["JPSM_BOT"].bot_token;

// Plugins
const general = require('./general.js');
const botLogger = require('./botLogger.js');
const music = require('./music.js');
const channelListener = require('./channelListener.js');
const guildListener = require('./guildListener.js');
const roles = require('./roles.js');
const league = require('./league.js');
const main = require('./main.js');
var prefix = ";";

// // Apply the main plugin.
// main(bot, {
//   prefix: `${prefix}`
// });

// Apply the general plugin.
general(bot, {
  prefix: `${prefix}`
});

// Apply the roles plugin.
roles(bot, {
  prefix: `${prefix}`
});

// Apply the league plugin.
league(bot, {
  prefix: `${prefix}`
});

// Apply the logger plugin.
botLogger(bot, {
  prefix: `${prefix}`
});

// Apply the channelListener plugin.
channelListener(bot, {
  prefix: `${prefix}`
});

// Apply the guildListener plugin.
guildListener(bot, {
  prefix: `${prefix}`
});

// Apply the music plugin.
music(bot, {
  prefix: `${prefix}`,
  maxQueueSize: 30,
  config: `${JSON.stringify(config)}`
});

// Login.
bot.login(token);
