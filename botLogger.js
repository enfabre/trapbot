const console = require('better-console');
const pmx = require('pmx');
/**
 * Always include this logger!
 * @param  {Client} bot     This the starting point Bot.
 * @param  {object} options Misc Options undefined for now
 * @return {Void}
 */
module.exports = function(bot, options) {
  let PREFIX = (options && options.prefix) || ';';

  bot.on('ready', () => {
    console.info(`I am ready! and currently running on ${bot.guilds.size} servers`);
    bot.user.setStatus('status', `Message ${PREFIX}help`)
      .then(user => console.log('Set status!'))
      .catch(console.log);
  });

  bot.on('error', e => {
    console.error(e);
    pmx.notify(e);
  });

  bot.on('warn', e => {
    console.warn(e);
  });

  bot.on('debug', e => {
    console.info(e);
  });

  process.on('exit', (code) => {
    console.log(`About to exit with code ${code}`);
  });

  process.on('uncaughtException', (error) => {
    if (error.code == 'ECONNRESET') {
      pmx.notify(`TrapBot Caught ${error}`);
      return;
    } else {
      console.log(`TrapBot Caught : ${error}`);
      pmx.notify(`TrapBot Caught : ${error}`);
    }
  });

}
