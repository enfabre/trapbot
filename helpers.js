const numeral = require('numeral');

var toTime = function(n) {
  return numeral((n / 1000) | 0).format('00:00:00');
}

var listUsers = function(onOrOffOrAll, strArray) {
  var strArr = strArray.array();
  var strToReturn = "\n";
  if (onOrOffOrAll === "on") {
    for (i = 0; i < strArr.length; i++) {
      if (strArr[i].status === "online" || strArr[i].status === "idle") {
        if (!strArr[i].bot) {
          strToReturn += strArr[i];
          strToReturn += "\n";
        }
      }
    }
  } else if (onOrOffOrAll === "off") {
    for (i = 0; i < strArr.length; i++) {
      if (strArr[i].status === "offline") {
        if (!strArr[i].bot) {
          strToReturn += strArr[i];
          strToReturn += "\n";
        }
      }
    }
  } else {
    for (i = 0; i < strArr.length; i++) {
      if (!strArr[i].bot) {
        strToReturn += strArr[i];
        strToReturn += "\n";
      }
    }
  }
  return strToReturn
}

var actualUsersSize = function(fullArr) {
  var strArr = fullArr.array();
  var actualUsers = 0;
  for (i = 0; i < strArr.length; i++) {
    if (!strArr[i].bot) {
      actualUsers++;
    }
  }
  return actualUsers;
}

var notifyGameAll = function(user, ingame) {
  var botServers = bot.guilds.array();
  for (i = 0; i < botServers.length; i++) {
    if (botServers[i].member(user) != null) {
      var serverChannels = botServers[i].channels.array();
      for (j = 0; j < serverChannels.length; j++) {
        if (serverChannels[j].type === "text") {
          if (ingame === true) {
            serverChannels[j].sendMessage(user + " Is playing **"
              + user.game.name + "**");
          } else {
            serverChannels[j].sendMessage(user + " stopped playing **"
              + user.game.name + "**");
          }
        }
      }
    }
  }
}


module.exports = {
  toTime: toTime,
  listUsers: listUsers,
  actualUsersSize: actualUsersSize,
  notifyGameAll: notifyGameAll
};
