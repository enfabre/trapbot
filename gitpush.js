const git = require('simple-git');

git()
  .add('./*')
  .commit("Code Updated")
  .push(['-u', 'origin', 'master'], () => console.log('Code pushed'));
