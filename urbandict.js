var urban = require('urban');
module.exports = class Urban {
  define(input) {
    return new Promise(function(resolve, reject) {
      var term = urban(input);
      term.first(function(json) {
        resolve(json);
      });
    });
  }

// term.first(function(json) {
//   console.log(json);
// });
}
