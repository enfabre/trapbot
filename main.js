const console = require('better-console');
const mysql = require('mysql');
const trapbot = require('./trapbot.js');

module.exports = function(bot, options) {
  let PREFIX = (options && options.prefix) || ';';

  bot.on('message', msg => {
    const message = msg.content.trim();

    // Check if the message is a command.
    if (message.startsWith(PREFIX)) {
      // Get the command and suffix.
      const command = message.split(/[ \n]/)[0].substring(PREFIX.length).toLowerCase().trim();
      const suffix = message.substring(PREFIX.length + command.length).trim();

      // Process the commands.
      switch (command) {
        case 'setprefix':
          return setprefix(msg, suffix);
        case 'getprefix':
          return getprefix(msg, suffix);
      }
    }
  });

  function setprefix(msg, suffix) {
    let server = msg.channel.guild;
  }

  function getprefix(msg, suffix) {
    msg.channel.sendMessage(`${PREFIX}`);
    let server = msg.channel.guild;
  }
}
