var cheerio = require('cheerio');
var selenium = require('selenium-webdriver');
var winston = require('winston');

var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.File)({
      name: 'info-file',
      filename: 'filelog-info.log',
      level: 'info'
    }),
    new (winston.transports.File)({
      name: 'error-file',
      filename: 'filelog-error.log',
      level: 'error'
    }),
    new (winston.transports.Console)({
      level: 'debug'
    })
  ]
});
var phantomjs_exe = require('phantomjs').path;

var customPhantom = selenium.Capabilities.phantomjs();
customPhantom.set("phantomjs.binary.path", phantomjs_exe);
// Declare 'driver', 'By' and URL globals.
var By = selenium.By,
  until = selenium.until,
  url = "https://na.op.gg/";
var mainHandle = null;


module.exports = class opgg {

  getInfo(user) {
    return new Promise(function(resolve, reject) {

      getDriver(user).then(function(driv) {
        driv.getPageSource().then(function(s) {

          let $ = cheerio.load(s)
          var playerName = $('span[class=Name]').text().trim();
          var lastUpdate = $('div[class=LastUpdate]').text().trim();
          var ladderRank = $('div[class=LadderRank]').text().trim();
          var duoRank = $('span[class=tierRank]').text().trim();
          var lp = $('span[class=LeaguePoints]').text().trim();
          var winrate = $('span[class=winratio]').first().text().trim();
          var res = `
**Summoner: ${playerName}
${ladderRank}
Duo Rank: ${duoRank}
LP: ${lp}
${winrate}
${lastUpdate}**`;
          resolve(res);
        })
      })
        .catch(function(err) {
          reject(err)
        });
    });
  }

  getmmr(user) {
    return new Promise(function(resolve, reject) {

      getDriver(user).then(function(driv) {
        var button = driv.wait(until.elementLocated(By.xpath('/html/body/div[1]/div[2]/div/div[3]/div/div[1]/div[3]/div[2]/button[3]'), 5000));
        button.click().then(function(src) {
          driv.wait(until.elementLocated(
            By.xpath('//*[@id="ExtraView"]/div/div[1]/table/tbody/tr[1]/td[1]'), 5000)).getText().then(function(blah) {
            resolve(`
**MMR**: ${blah}`);
          });
        });
      });
    });
  }
}

function getDriver(user) {
  return new Promise(function(resolve, reject) {
    // Connects to url
    var driver = new selenium.Builder().withCapabilities(customPhantom).build();
    console.log(user);
    // driver.get(`https://na.op.gg/summoner/userName=Royalt%C3%BD`);
    var url_encoded = `${url}summoner/userName=${user}`;
    driver.get(encodeURI(url_encoded));
    driver.getCurrentUrl().then(function(u) {
      logger.debug(`Attempting to connect to ${u}`);
      driver.getPageSource().then(function(source) {
        if (source.includes("This summoner is not registered at OP.GG.")) {
          reject("Summoner Does Not Exist")
        } else {
          var refreshButton = driver.wait(until.elementLocated(By.id('SummonerRefreshButton'), 5000));
          refreshButton.getText().then(function(sol) {
            logger.info(sol)
          })
          refreshButton.click().then(function() {
            resolve(driver);
          });
        }
      });

    });
  });
}

function closeDriver() {
  return new Promise(function(resolve, reject) {
    driver.close().then(function() {
      resolve(true)
    })
      .catch(function(err) {
        reject(err)
      })
  });
}
