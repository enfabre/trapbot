const console = require('better-console');
const Helper = require('./helpers.js')
const mysql = require('mysql');

// Helper Req's
const toTime = Helper.toTime;
const listUsers = Helper.listUsers;
const actualUsersSize = Helper.actualUsersSize;
const notifyGameAll = Helper.notifyGameAll;

/**
 * Adds a music feature to your bot.
 * @param  {Client} bot     This the starting point Bot.
 * @param  {object} options Misc Options undefined for now
 * @return {Void}
 */
module.exports = function(bot, options) {
  // Get all options.
  let PREFIX = (options && options.prefix) || ';';

  bot.on('message', msg => {
    const message = msg.content.trim();

    // Check if the message is a command.
    if (message.startsWith(PREFIX)) {
      // Get the command and suffix.
      const command = message.split(/[ \n]/)[0].substring(PREFIX.length).toLowerCase().trim();
      const suffix = message.substring(PREFIX.length + command.length).trim();

      // Process the commands.
      switch (command) {
        case 'roles':
          return roles(msg, suffix);
        case 'getrank':
          return getrank(msg, suffix);
      }
    }
  });
  /**
   * Returns roles within a server
   * @param  {message} msg    [description]
   * @param  {message} suffix [description]
   * @return {void}        [description]
   */
  function roles(msg, suffix) {
    msg.channel.sendMessage(serverRolesGetter(msg.guild));
  }

  /**
   * Returns the max rank of the user
   * @param  {message} msg    [description]
   * @param  {message} suffix [description]
   * @return {void}        [description]
   */
  function getrank(msg, suffix) {
    let mentioneduser = msg.guild.member(msg.mentions.users.first());
    let memberRoles = memberRolesGetter(mentioneduser);
    console.log(memberRoles);
    msg.channel.sendMessage(`${mentioneduser} is ranked ${memberRoles[memberRoles.length - 1]}`);
  }

  /**
   * Returns roles of member
   * @param  {GuildMember} member Discord guild member
   * @return {array}        An array of discord roles.
   */
  function serverRolesGetter(guild) {
    let sRoles = [];
    guild.roles.forEach((role) => {
      sRoles[role.position] = role;
    });
    return sRoles.reverse();
  }


  Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
      if (this[i] == deleteValue) {
        this.splice(i, 1);
        i--;
      }
    }
    return this;
  };
}
