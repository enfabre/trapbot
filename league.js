const console = require('better-console');
const Helper = require('./helpers.js')
const mysql = require('mysql');
const opgg = require('./opgg');
const Counters = require('./counters.js');

const League = new opgg();
const counter = new Counters();

// Helper Req's
const toTime = Helper.toTime;
const listUsers = Helper.listUsers;
const actualUsersSize = Helper.actualUsersSize;
const notifyGameAll = Helper.notifyGameAll;

/**
 * Adds a music feature to your bot.
 * @param  {Client} bot     This the starting point Bot.
 * @param  {object} options Misc Options undefined for now
 * @return {Void}
 */
module.exports = function(bot, options) {
  // Get all options.
  let PREFIX = (options && options.prefix) || ';';

  // Create an object of queue.
  let queue = {};

  bot.on('message', msg => {
    const message = msg.content.trim();

    // Check if the message is a command.
    if (message.startsWith(PREFIX)) {
      // Get the command and suffix.
      const command = message.split(/[ \n]/)[0].substring(PREFIX.length).toLowerCase().trim();
      const suffix = message.substring(PREFIX.length + command.length).trim();

      // Process the commands.
      switch (command) {
        case 'getmmr':
          return getmmr(msg, suffix);
        case 'getinfo':
          return getInfo(msg, suffix);
        case 'champinfo':
          return champInfo(msg, suffix);
        case 'counters':
          return counters(msg, suffix);

      }
    }
  });

  function counters(msg, suffix) {
    if (suffix.includes('ed') && suffix.match(/(ed\w+)/) == null) {
      return msg.reply(`
__**The Chief**_ is:
~~**Weak Against**~~

**Strong Against**
**Level** = God-Like
`)
    }
    if (suffix.includes('wil') && suffix.match(/(wil\w+)/) == null) {
      return msg.reply(`
__**Wilyion**__ is:
**Weak Against**
Cheese

**Strong Against**
Thots
`)
    }
    if (suffix.includes('rico') && suffix.match(/(rico\w+)/) == null) {
      return msg.reply(`
__**GordoRicon**__ is:
**Weak Against**
Thots

**Strong Against**
Ho's
`)
    }
    if (suffix.includes('50') && suffix.match(/(50\w+)/) == null) {
      return msg.reply(`
__**50**__ is:
**Weak Against**
Man is called 50... No weaknesses.

**Strong Against**
Man is called 50........
`)
    }
    if (suffix.includes('david') && suffix.match(/(david\w+)/) == null) {
      return msg.reply(`
__**David**__ is:
**Weak Against**
League of Legends

**Strong Against**
IDK......
`)
    }
    counter.getWeakStrongAgainst(suffix).then(function(sol) {
      return msg.reply(sol);
    })
      .catch(function(err) {
        return msg.reply('Try again!');
        console.error(err);
      });
  }

  function champInfo(msg, suffix) {
    counter.getChampInfo(suffix).then(function(sol) {
      msg.reply(sol);
    })
      .catch(function(err) {
        msg.reply('Try again!');
        console.error(err);
      });
  }

  function getInfo(msg, suffix) {
    // Start Typing animation in discord
    msg.channel.startTyping();
    console.log(suffix);
    League.getInfo(suffix).then(function(sol) {
      msg.channel.stopTyping();
      msg.reply(sol);
    })
      .catch(function(err) {
        msg.channel.stopTyping();
        msg.reply('Try again!');
        console.error(err);
      });
  }

  function getmmr(msg, suffix) {
    // Start Typing animation in discord
    msg.channel.startTyping();

    League.getmmr(suffix).then(function(sol) {
      msg.channel.stopTyping();
      msg.reply(sol);
    })
      .catch(function(err) {
        msg.channel.stopTyping();
        msg.reply('Try again!');
        console.error(err);
      });
  }
}
